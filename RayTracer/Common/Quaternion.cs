﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Quaternion: IVector, ICloneable
    {
        public double W { get; private set; }
        public Point3D Axis { get; private set; }

        public Quaternion()
        {
            W = 0;
            Axis = new Point3D(0, 0, 1);
        }

        public Quaternion(double x, double y, double z, double angle)
        {
            W = 0;
            Axis = new Point3D();

            angle /= 2;
            double sin = Math.Sin(angle);

            W = Math.Cos(angle);
            Axis.x = sin * x;
            Axis.y = sin * y;
            Axis.z = sin * z;
        }

        public Quaternion(Point3D offset, double angle)
        {
            W = 0;
            Axis = new Point3D();

            angle /= -2;
            double sin = Math.Sin(angle);

            W = Math.Cos(angle);
            Axis.x = sin * offset.x;
            Axis.y = sin * offset.y;
            Axis.z = sin * offset.z;
        }

        public Quaternion(Point3D p)
        {
            W = 0;
            Axis = (Point3D) p.Clone();
        }

        public object Clone()
        {
            return new Quaternion()
            {
                W = this.W,
                Axis = (Point3D) this.Axis.Clone()
            };
        }

        public double Scalar()
        {
            return Math.Sqrt(W * W + Axis.x * Axis.x + Axis.y * Axis.y + Axis.z * Axis.z);
        }

        public void Normalize()
        {
            double scalar = Scalar();

            W /= scalar;
            Axis /= scalar;
        }

        public Quaternion Conjugation()
        {
            return new Quaternion()
            {
                W = this.W,
                Axis = -((Point3D) this.Axis.Clone())
            };
        }

        public Quaternion Inverse()
        {
            var res = Conjugation();
            res.Normalize();

            return res;
        }

        public Quaternion Multiply(Quaternion q)
        {
            double coef;
            Point3D longAxis1 = null, longAxis2 = null;

            if (CommonFunctions.CompareDouble(W, 0))
            {
                coef = 0;
                longAxis2 = new Point3D();
            }
            else
            {
                coef = W * q.W;
                longAxis2 = q.Axis * W;
            }

            if (CommonFunctions.CompareDouble(q.W, 0))
                longAxis1 = new Point3D();
            else
                longAxis1 = Axis * q.W;

            Point3D resAxis = longAxis1 + longAxis2;

            Quaternion res = new Quaternion();
            res.W = coef - (Axis.x * q.Axis.x) - (Axis.y * q.Axis.y) - (Axis.z * q.Axis.z);
            res.Axis.x = resAxis.x + (Axis.y * q.Axis.z) - (Axis.z * q.Axis.y);
            res.Axis.y = resAxis.y - (Axis.x * q.Axis.z) + (Axis.z * q.Axis.x);
            res.Axis.z = resAxis.z + (Axis.x * q.Axis.y) - (Axis.y * q.Axis.x);

            return res;
        }
    }
}
