﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public static class CommonFunctions
    {
        public static double Sqr(double x)
        {
            return x * x;
        }

        public static bool CompareDouble (double a, double b)
        {
            const double eps = 1e-7;

            return Math.Abs(a - b) < eps;
        }

        public static double[] SolveQuadraticEquation(double a, double b, double c)
        {
            if (a == 0)
            {
                if (b == 0)
                    return null;
                else if (c == 0)
                    return new double[] { 0 };
                else
                    return new double[] { -c / b };
            }

            double discriminant = Sqr(b) - 4 * a * c;

            if (discriminant < 0)
                return null;

            a *= 2;
            b = -b;

            if (CompareDouble(discriminant, 0))
                return new double[] { b / a };

            discriminant = Math.Sqrt(discriminant);

            return new double[]
            {
                (b - discriminant) / a,
                (b + discriminant) / a
            };
        }
    }
}
