﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Point3D: ISceneObject, ICloneable
    {
        public double x = 0, y = 0, z = 0;

        public Point3D()
        {
        }

        public Point3D(double x, double y, double z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public Point3D(string coords)
        {
            var val = coords.Split(' ');

            x = Convert.ToDouble(val[0]);
            y = Convert.ToDouble(val[1]);
            z = Convert.ToDouble(val[2]);
        }

        public static Point3D operator+ (Point3D a, Point3D b)
        {
            return new Point3D()
            {
                x = a.x + b.x,
                y = a.y + b.y,
                z = a.z + b.z
            };
        }

        public static Point3D operator- (Point3D a)
        {
            return new Point3D()
            {
                x = -a.x,
                y = -a.y,
                z = -a.z
            };
        }

        public static Point3D operator- (Point3D a, Point3D b)
        {
            return a + (-b);
        }

        public static Point3D operator* (Point3D a, double b)
        {
            return new Point3D()
            {
                x = a.x * b,
                y = a.y * b,
                z = a.z * b
            };
        }

        public static Point3D operator* (Point3D a, Point3D b)
        {
            return new Point3D
            (
                a.y * b.z - a.z * b.y,
                a.z * b.x - a.x * b.z,
                a.x * b.y - a.y * b.x
            );
        }

        public static Point3D operator/ (Point3D a, double b)
        {
            if (CommonFunctions.CompareDouble(b, 0))
                throw new DivideByZeroException();

            return new Point3D()
            {
                x = a.x / b,
                y = a.y / b,
                z = a.z / b
            };
        }

        public static double DotProduct(Point3D a, Point3D b)
        {
            return (a.x * b.x + a.y * b.y + a.z * b.z);
        }

        public static bool operator== (Point3D a, Point3D b)
        {
            if (Object.ReferenceEquals(a, null) || Object.ReferenceEquals(b, null))
                return Object.ReferenceEquals(a, b);

            return  CommonFunctions.CompareDouble(a.x, b.x) &&
                    CommonFunctions.CompareDouble(a.y, b.y) &&
                    CommonFunctions.CompareDouble(a.z, b.z);
        }

        public static bool operator!= (Point3D a, Point3D b)
        {
            return !(a == b);
        }

        public static double Distance(Point3D a, Point3D b)
        {
            return Math.Sqrt (CommonFunctions.Sqr(a.x - b.x) + CommonFunctions.Sqr (a.y - b.y) + CommonFunctions.Sqr(a.z - b.z));
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() == typeof(Point3D))
                return this == (Point3D)obj;
            else
                return base.Equals(obj);
        }

        public void Move (double dx, double dy, double dz)
        {
            x += dx;
            y += dy;
            z += dz;
        }

        public void Rotate (Quaternion q, Quaternion invQ)
        {
            var temp = q.Multiply(new Quaternion(this)); // rot_p = q * p * (q^-1)
            var res = temp.Multiply(invQ);


            x = res.Axis.x;
            y = res.Axis.y;
            z = res.Axis.z;
        }

        public double Scalar()
        {
            return Math.Sqrt(x * x + y * y + z * z);
        }

        public object Clone()
        {
            return new Point3D()
            {
                x = this.x,
                y = this.y,
                z = this.z
            };
        }
    }
}