﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Plane
    {
        public Plane (Point3D p1, Point3D p2, Point3D p3)
        {
            Line line = null;

            try
            {
                line = new Line(p1, p2);
            }
            catch (ArgumentException)
            {
                throw new ArgumentException("Two points are equal in plane constructor!\n");
            }

            if (line.Check(p3) == true)
                throw new ArgumentException("All three points are on the single line in plane constructor!\n");

            const int rows = 2, cols = 3;
            double[][] coefMatrix = new double[rows][]
            {
                new double[cols] { p2.x - p1.x, p2.y - p1.y, p2.z - p1.z },
                new double[cols] { p3.x - p1.x, p3.y - p1.y, p3.z - p1.z}
            };

            double temp = Determinant(coefMatrix[0][1], coefMatrix[0][2], coefMatrix[1][1], coefMatrix[1][2]);
            a = temp;
            d += -p1.x * temp;

            temp = Determinant(coefMatrix[0][0], coefMatrix[0][2], coefMatrix[1][0], coefMatrix[1][2]);
            b = temp;
            d += p1.y * temp;

            temp = Determinant(coefMatrix[0][0], coefMatrix[0][1], coefMatrix[1][0], coefMatrix[1][1]);
            c = temp;
            d += -p1.z * temp;

            denominant = Math.Sqrt(a * a + b * b + c * c);

            var normal = new Point3D(a, b, c);
            normal /= normal.Scalar();

            Normal = normal;
        }

        public bool CheckPoint(Point3D p)
        {
            return CommonFunctions.CompareDouble(Distance(p), 0);
        }

        public double Distance(Point3D p)
        {
            double val = a * p.x + b * p.y + c * p.z + d;

            return val / denominant;
        }

        public Point3D Intersects(Line l)
        {
            var vec = l.CoefsVector;

            var bPoint = vec.rVec + vec.offset;

            if (CommonFunctions.CompareDouble(Distance(vec.rVec), Distance(bPoint)))
                return null;

            double tx = a * vec.offset.x;
            double ty = b * vec.offset.y;
            double tz = c * vec.offset.z;

            double tVal = a * vec.rVec.x + b * vec.rVec.y + c * vec.rVec.z + d;

            double t = -tVal / (tx + ty + tz);

            var res = new Point3D()
            {
                x = vec.offset.x * t + vec.rVec.x,
                y = vec.offset.y * t + vec.rVec.y,
                z = vec.offset.z * t + vec.rVec.z
            };

            return res;
        }

        public readonly Point3D Normal;

        private double Determinant(double a, double b, double c, double d)
        {
            return a * d - b * c;
        }

        private double a = 0, b = 0, c = 0, d = 0;
        private double denominant;
    }
}
