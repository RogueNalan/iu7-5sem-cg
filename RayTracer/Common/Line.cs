﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Line: ISceneObject
    {
        public Line (Vector3D vec)
        {
            CoefsVector = (Vector3D) vec.Clone();

            CalculateCache();

            if (CheckCache() == false)
                throw new ArgumentException("Two points are equal in Line constructor!\n");
        }

        public Line (Point3D a, Point3D b)
        {
            CoefsVector = new Vector3D(a, b);

            CalculateCache();

            if (CheckCache() == false)
                throw new ArgumentException("Two points are equal in Line constructor!\n");
        }

        public void Move (double dx, double dy, double dz)
        {
            CoefsVector.Move(dx, dy, dz);
        }

        public void Rotate (Quaternion q, Quaternion invQ)
        {
            CoefsVector.Rotate(q, invQ);

            CalculateCache();
        }

        public bool Check(Point3D p)
        {
            double? xVal = null, yVal = null, zVal = null;

            if (cachedCheck[0] == true)
                xVal = (p.x - CoefsVector.rVec.x) / (CoefsVector.offset.x);
            else if (!CommonFunctions.CompareDouble(p.x, CoefsVector.rVec.x))
                return false;

            if (cachedCheck[1] == true)
                yVal = (p.y - CoefsVector.rVec.y) / (CoefsVector.offset.y);
            else if (!CommonFunctions.CompareDouble(p.y, CoefsVector.rVec.y))
                return false;

            if (cachedCheck[2] == true)
                zVal = (p.z - CoefsVector.rVec.z) / (CoefsVector.offset.z);
            else if (!CommonFunctions.CompareDouble(p.z, CoefsVector.rVec.z))
                return false;

            if (CompareNullTrue(xVal, yVal) && CompareNullTrue(xVal, zVal) && CompareNullTrue(yVal, zVal))
                return true;
            else
                return false;
        }

        private void CalculateCache()
        {
            for (int i = 0; i < cachedCheck.Length; ++i)
                cachedCheck[i] = true;

            if (CommonFunctions.CompareDouble(CoefsVector.offset.x, 0))
                cachedCheck[0] = false;

            if (CommonFunctions.CompareDouble(CoefsVector.offset.y, 0))
                cachedCheck[1] = false;

            if (CommonFunctions.CompareDouble(CoefsVector.offset.z, 0))
                cachedCheck[2] = false;
        }

        private bool CheckCache()
        {
            foreach (bool b in cachedCheck)
                if (b)
                    return true;

            return false;
        }

        private static bool CompareNullTrue (double? a, double? b)
        {
            if ((a == null) || (b == null))
                return true;

            if (CommonFunctions.CompareDouble(a.Value, b.Value))
                return true;
            else
                return false;
        }

        public Vector3D CoefsVector { get; private set; }

        private bool[] cachedCheck = new bool[] { true, true, true };
    }
}
