﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Vector3D : ISceneObject, IVector,  ICloneable
    {
        public Point3D rVec;
        public Point3D offset;

        public Vector3D()
        {
            rVec = new Point3D();
            offset = new Point3D();
        }

        public Vector3D(Point3D a, Point3D b)
        {
            rVec = (Point3D)a.Clone();
            offset = new Point3D()
            {
                x = b.x - a.x,
                y = b.y - a.y,
                z = b.z - a.z
            };
        }

        public static Vector3D operator +(Vector3D a, Vector3D b)
        {
            return new Vector3D()
            {
                rVec = a.rVec + b.rVec,
                offset = a.offset + b.offset
            };
        }

        public static Vector3D operator -(Vector3D a)
        {
            return new Vector3D()
            {
                rVec = a.rVec,
                offset = -a.offset
            };
        }

        public static Vector3D operator -(Vector3D a, Vector3D b)
        {
            return a + (-b);
        }
        
        public static double operator *(Vector3D a, Vector3D b)
        {
            return a.offset.x * b.offset.x + a.offset.y * b.offset.y + a.offset.z * b.offset.z;
        }

        public static Vector3D operator * (Vector3D a, double b)
        {
            return new Vector3D()
            {
                rVec = (Point3D) a.rVec.Clone(),
                offset = a.offset * b
            };

        }

        public static Vector3D operator / (Vector3D a, double b)
        {
            return new Vector3D()
            {
                rVec = (Point3D) a.rVec.Clone(),
                offset = a.offset / b
            };
        }

        public double GetAngleCos(Vector3D v)
        {
            double val = offset.x * v.offset.x + offset.y * v.offset.y + offset.z * v.offset.z;

            return val / Scalar() / v.Scalar();
        }

        public bool Collinear(Vector3D v)
        {
            double val = GetAngleCos(v);

            return CommonFunctions.CompareDouble(Math.Abs(val), 1);
        }

        public void Move (double dx, double dy, double dz)
        {
            rVec.x += dx;
            rVec.y += dy;
            rVec.z += dz;
        }
        
        public void Rotate (Quaternion q, Quaternion invQ)
        {
            rVec.Rotate(q, invQ);
            offset.Rotate(q, invQ);
        }

        public void Normalize()
        {
            offset /= Scalar();
        }

        public double Scalar()
        {
            return Math.Sqrt(offset.x * offset.x + offset.y * offset.y + offset.z * offset.z);
        }

        public object Clone()
        {
            return new Vector3D()
            {
                rVec = (Point3D)this.rVec.Clone(),
                offset = (Point3D)this.offset.Clone()
            };
        }
    }
}
