﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public interface IVector
    {
        double Scalar();
        void Normalize();
    }

    public interface ISceneObject
    {
        void Move(double dx, double dy, double dz);
        void Rotate(Quaternion q, Quaternion invQ);
    }
}
