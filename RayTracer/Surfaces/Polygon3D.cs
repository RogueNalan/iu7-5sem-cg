﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Common;

namespace Surfaces
{
    public class Polygon3D: IOneIntersection
    {
        public Polygon3D (Point3D a, Point3D b, Point3D c)
        {
            try
            {
                plane = new Plane(a, b, c);
            }
            catch (ArgumentException)
            {
                throw new ArgumentException("Polygon creation failed!\n");
            }

            Points = new Point3D[dimension];
            Points[0] = (Point3D)a.Clone();
            Points[1] = (Point3D)b.Clone();
            Points[2] = (Point3D)c.Clone();

            CalcCoefs();
        }

        public void Rotate(Quaternion q, Quaternion invQ)
        {
            foreach (Point3D p in Points)
                p.Rotate(q, invQ);

            plane = new Plane(Points[0], Points[1], Points[2]);

            CalcCoefs();
        }

        public void Move (double x, double y, double z)
        {
            foreach (Point3D p in Points)
                p.Move(x, y, z);

            plane = new Plane(Points[0], Points[1], Points[2]);

            CalcCoefs();
        }

        public RayData Intersects(Line l)
        {
            var res = plane.Intersects(l);

            if ((res != null) && (CheckSamePlane(res)))
                return new RayData(res, Normal (res));
            else
                return null;
        }

        public bool Check(Point3D p)
        {
            Polygon3D p1, p2, p3;

            try
            {
                p1 = new Polygon3D(Points[0], Points[1], p);
                p2 = new Polygon3D(Points[0], Points[2], p);
                p3 = new Polygon3D(Points[1], Points[2], p);
            }
            catch (ArgumentException)
            {
                return false;
            }

            double res = p1.Square + p2.Square + p3.Square;

            return CommonFunctions.CompareDouble(res, Square);
        }

        public bool CheckSamePlane(Point3D p)
        {
            /*double[] val = new double[dimension];

            for (int i = 0; i < dimension; ++i)
                val[i] = (Points[i].x - p.x) * yCoefs[i] - (Points[i].y - p.y) * xCoefs[i];

                foreach (double d in val)
                    if (CommonFunctions.CompareDouble(d, 0))
                        return false;

            int signVal = Math.Sign(val[1]);
            return (Math.Sign(val[0]) == signVal) && (Math.Sign(val[2]) == signVal);*/

            foreach (Point3D lim in Points)
                if (p == lim)
                    return true;

            Point3D[] offsets = new Point3D[dimension];
            for (int i = 0; i < dimension; ++i)
                offsets[i] = p - Points[i];


            double[] scalars = new double[dimension];
            for (int i = 0; i < dimension; ++i)
                scalars[i] = offsets[i].Scalar();

            Func<Point3D, Point3D, double, double, double> GetArcCos = 
                (a, b, aScal, bScal) => 
                        Math.Acos((a.x * b.x + a.y * b.y + a.z * b.z) / aScal / bScal);

            double sum = 0;
            for (int i = 1; i < dimension; ++i)
                sum += GetArcCos(offsets[i - 1], offsets[i], scalars[i - 1], scalars[i]);

            sum += GetArcCos(offsets[0], offsets[2], scalars[0], scalars[2]);

            const double val = 2 * Math.PI;

            return CommonFunctions.CompareDouble(sum, val);
        }

        public double Square
        {
            get
            {
                if (_square == null)
                    CalcSquare();
                return _square.Value;
            }
            private set
            {
                _square = value;
            }
        }

        public Point3D Normal (Point3D p)
        {
            return plane.Normal;
        }

        public Point3D[] Points { get; private set; }

        double? _square = null;

        private void CalcSquare()
        {
            double a = (Points[1] - Points[0]).Scalar();
            double b = (Points[2] - Points[0]).Scalar();
            double c = (Points[2] - Points[1]).Scalar();

            double p = (a + b + c) / 2;

            _square = Math.Sqrt(p * (p - a) * (p - b) * (p - c));
        }

        private static double CalcCheckValue (Point3D test, Point3D curr, Point3D next)
        {
            return (curr.x - test.x) * (next.y - curr.y) - (next.x - curr.x) * (curr.y - test.y);
        }

        private Plane plane { get; set; }

        private const int dimension = 3;

        private void CalcCoefs()
        {
            for (int i = 0; i < dimension - 1; ++i)
            {
                xCoefs[i] = Points[i + 1].x - Points[i].x;
                yCoefs[i] = Points[i + 1].y - Points[i].y;
            }

            xCoefs[2] = Points[0].x - Points[2].x;
            yCoefs[2] = Points[0].y - Points[2].y;
        }

        private double[] xCoefs = new double[dimension];
        private double[] yCoefs = new double[dimension];
    }
}
