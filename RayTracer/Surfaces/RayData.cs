﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

using Common;

namespace Surfaces
{
    public class RayData: ICloneable
    {
        public RayData()
        {
            point = new Point3D();
            normal = new Point3D();
        }

        public RayData(Point3D point, Point3D normal)
        {
            this.point = point;
            this.normal = normal;
        }

        public object Clone()
        {
            return new RayData((Point3D) point.Clone(), (Point3D) normal.Clone());
        }

        public Point3D point;
        public Point3D normal;
    }
}
