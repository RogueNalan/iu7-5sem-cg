﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Common;

namespace Surfaces
{
    public class Sphere: ISurface
    {   
        public Sphere(Point3D center, double rad)
        {

            if (rad < eps)
                throw new ArgumentException("Sphere radius cannot be less than zero!\n");

            this.center = (Point3D) center.Clone();
            this.rad = rad;
        }

        public Sphere(double x, double y, double z, double rad)
        {
            if (rad < eps)
                throw new ArgumentException("Sphere radius cannot be less than zero!\n");

            center = new Point3D(x, y, z);
            this.rad = rad;
        }

        public void Move (double dx, double dy, double dz)
        {
            center.x += dx;
            center.y += dy;
            center.z += dz;
        }

        public void Rotate (Quaternion q, Quaternion invQ)
        {
            center.Rotate(q, invQ);
        }

        public Point3D[] Intersects(Line l)
        {
            var rVec = l.CoefsVector.rVec;
            var offset = l.CoefsVector.offset;

            double a = CommonFunctions.Sqr(offset.x) + CommonFunctions.Sqr(offset.y) + CommonFunctions.Sqr(offset.z);
            double b = 0, c = 0;

            double[] values = new double[]
            {
                l.CoefsVector.rVec.x - center.x,
                l.CoefsVector.rVec.y - center.y,
                l.CoefsVector.rVec.z - center.z
            };

            b += values[0] * offset.x;
            b += values[1] * offset.y;
            b += values[2] * offset.z;
            b *= 2;

            foreach (double x in values)
                c += CommonFunctions.Sqr(x);

            c -= CommonFunctions.Sqr(rad);

            Point3D[] res = null;

            double[] roots = CommonFunctions.SolveQuadraticEquation(a, b, c);

            if (roots == null)
                return null;

            res = new Point3D[roots.Length];

            for (int i = 0; i < roots.Length; ++i)
            {
                res[i] = new Point3D
                (
                    offset.x * roots[i] + rVec.x,
                    offset.y * roots[i] + rVec.y,
                    offset.z * roots[i] + rVec.z
                );
            }

            return res;
        }

        public bool Check (Point3D p)
        {
            double val = CommonFunctions.Sqr(p.x - center.x) + CommonFunctions.Sqr(p.y - center.y) + CommonFunctions.Sqr(p.z - center.z);

            return CommonFunctions.CompareDouble(val, CommonFunctions.Sqr(rad));
        }

        public Point3D Normal(Point3D p)
        {
            var vec = new Vector3D(center, p);
            vec.Normalize();

            return vec.offset;
        }

        private Point3D center;
        private double rad;
        private const double eps = 1e-7;
    }
}
