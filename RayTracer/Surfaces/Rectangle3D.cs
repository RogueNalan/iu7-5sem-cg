﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;

namespace Surfaces
{
    public class Rectangle3D: IOneIntersection
    {
        public Rectangle3D(Point3D a, Point3D b, Point3D c)
        {
            double ab = Point3D.Distance(a, b);
            double bc = Point3D.Distance(b, c);
            double ac = Point3D.Distance(a, c);

            if ((ab > bc) && (ab > ac))
            {
                A = (Point3D) a.Clone();
                B = (Point3D) c.Clone();
                C = (Point3D) b.Clone();
            }
            else if ((ac > ab) && (ac > bc))
            {
                A = (Point3D)a.Clone();
                B = (Point3D)b.Clone();
                C = (Point3D)c.Clone();
            }
            else
            {
                A = (Point3D)b.Clone();
                B = (Point3D)a.Clone();
                C = (Point3D)c.Clone();
            }

            D = A + (C - B);

            var vec1 = new Vector3D(A, B);
            var vec2 = new Vector3D(B, C);

            if (!CommonFunctions.CompareDouble(vec1.GetAngleCos(vec2), 0))
                throw new ArgumentException("Wrong points in parallelipiped creation!\n");

            try
            {
                plane = new Plane(A, B, C);
            }
            catch (ArgumentException)
            {
                throw new ArgumentException("Wrong points in parallelipiped creation!\n");
            }

            checkPolygons = new Polygon3D[]
            {
                new Polygon3D(A, B, C),
                new Polygon3D(A, D, C)
            };
        }

        public RayData Intersects(Line l)
        {
            Point3D res = plane.Intersects(l);

            if ((res != null) && CheckSamePlane(res))
                return new RayData(res, Normal(res));
            else
                return null;
        }

        public bool Check (Point3D p)
        {
            if ((p == A) || (p == B) || (p == C) || (p == D))
                return false;

            Polygon3D[] polygons = null;
            try
            {
                const int size = 4;
                polygons = new Polygon3D[size]
                {
                    new Polygon3D(A, B, p),
                    new Polygon3D(B, C, p),
                    new Polygon3D(C, D, p),
                    new Polygon3D(A, D, p)
                };
            }
            catch (ArgumentException)
            {
                return false;
            }

            double val = 0;

            foreach (Polygon3D x in polygons)
                val += x.Square;

            if (CommonFunctions.CompareDouble(val, Square))
                return true;
            else
                return false;
        }

        public void Move(double x, double y, double z)
        {
            A.Move(x, y, z);
            B.Move(x, y, z);
            C.Move(x, y, z);
            D.Move(x, y, z);

            plane = new Plane(A, B, C);

            foreach (Polygon3D p in checkPolygons)
                p.Move(x, y, z);
        }

        public void Rotate(Quaternion q, Quaternion invQ)
        {
            A.Rotate(q, invQ);
            B.Rotate(q, invQ);
            C.Rotate(q, invQ);
            D.Rotate(q, invQ);

            plane = new Plane(A, B, C);

            foreach (Polygon3D p in checkPolygons)
                p.Rotate(q, invQ);
        }
        
        public Point3D Normal (Point3D p)
        {
            return plane.Normal;
        }

        private void CalcSquare()
        {
            Square = Point3D.Distance(A, B) * Point3D.Distance(B, C);
        }

        public bool CheckSamePlane (Point3D p)
        {
            foreach (Polygon3D check in checkPolygons)
                if (check.CheckSamePlane(p))
                    return true;
            return false;
        }

        public Point3D A { get; private set; }
        public Point3D B { get; private set; }
        public Point3D C { get; private set; }
        public Point3D D { get; private set; }

        public double Square
        {
            get
            {
                if (_square == null)
                    CalcSquare();
                return _square.Value;
            }
            private set
            {
                _square = value;
            }
        }

        private Plane plane { get; set; }
        private double? _square;

        private Polygon3D[] checkPolygons = null;
    }
}
