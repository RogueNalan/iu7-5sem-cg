﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Common;

namespace Surfaces
{
    public interface ISurface
    {
        bool Check(Point3D p);

        void Move(double dx, double dy, double dz);
        void Rotate(Quaternion q, Quaternion invQ);
        Point3D Normal(Point3D p);
    }

    public interface IOneIntersection: ISurface
    {
        RayData Intersects(Line l);
    }
}
