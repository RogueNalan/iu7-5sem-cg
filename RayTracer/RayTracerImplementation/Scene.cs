﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Common;
using DrawObjects;

namespace RayTracerImplementation
{
    internal class Scene
    {
        public List<DrawObject> Objects     { get; private set; }
        public List<Point3D> LightSources   { get; private set; }

        public Scene()
        {
            Objects = new List<DrawObject>();
            LightSources = new List<Point3D>();
        }

    }
}
