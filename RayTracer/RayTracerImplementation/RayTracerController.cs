﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

using Common;
using DrawObjects;

namespace RayTracerImplementation
{
    public class RayTracerController
    {
        public RayTracerController(PictureBox pbImage, int width, int height)
        {
            if (pbImage == null)
                throw new ArgumentException("Image place to draw cannot be null!\n");

            this.image = pbImage;
            camera = new Camera(aspectAngle, width, height);
        }

        public void Render()
        {
            image.Image = RayTracer.TracePicture(camera, scene);
            image.Invalidate();
        }

        public void AddDrawObject(DrawObject d)
        {
            scene.Objects.Add(d);
        }

        public void AddLightSource(Point3D ls)
        {
            scene.LightSources.Add(ls);
        }

        public void DeleteObject (int id)
        {
            foreach (DrawObject obj in scene.Objects)
                if (obj.Id == id)
                {
                    scene.Objects.Remove(obj);
                    break;
                }
        }

        public bool MoveObject(int id, double dx, double dy, double dz)
        {
            foreach (DrawObject obj in scene.Objects)
                if (obj.Id == id)
                {
                    obj.Move(dx, dy, dz);
                    return true;
                }
            return false;
        }

        public bool RotateObject(int id, double x, double y, double z, double angle)
        {
            angle *= Math.PI / 180;

            foreach (DrawObject obj in scene.Objects)
                if (obj.Id == id)
                {
                    var q = new Quaternion(x, y, z, angle);
                    q.Normalize();
                    var invQ = q.Conjugation();

                    obj.Rotate(q, invQ);
                    return true;
                }
            return false;
        }

        public void MoveAll(double dx, double dy, double dz)
        {
            foreach (DrawObject obj in scene.Objects)
                obj.Move(dx, dy, dz);
        }

        public void RotateAll(double x, double y, double z, double angle)
        {
            angle *= Math.PI / 180;

            var q = new Quaternion(x, y, z, angle);
            q.Normalize();
            var invQ = q.Conjugation();

            foreach (DrawObject obj in scene.Objects)
                obj.Rotate(q, invQ);
        }

        private const double aspectAngle = (double) 80 / 180 * Math.PI;

        private Camera camera = null;
        private PictureBox image = null;
        private Scene scene = new Scene();
    }
}
