﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Threading;

namespace RayTracerImplementation
{
    internal class ThreadSafeBitmap: IDisposable
    {
        public ThreadSafeBitmap(int width, int height)
        {
            Value = new Bitmap(width, height);
        }

        public void SetPixel(int i, int j, Color color)
        {
            mutex.WaitOne();
            Value.SetPixel(i, j, color);
            mutex.ReleaseMutex();
        }

        public Bitmap Value { get; private set; }
        private Mutex mutex = new Mutex();

        public void Dispose()
        {
            mutex.Close();
        }
    }
}
