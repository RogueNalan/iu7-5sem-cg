﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;

using Common;
using DrawObjects;

namespace RayTracerImplementation
{
    internal static class RayTracer
    {
        public static Bitmap TracePicture (Camera camera, Scene scene)
        {
            var res = new Bitmap(camera.Width, camera.Height);

            int minX = -camera.Width / 2, maxX = -minX - 1,
                minY = -camera.Height / 2, maxY = -minY - 1;

            var startPoint = new Point3D();
            var tracePoint = new Point3D(minX, minY, camera.Z);

            /*var coordGetter = new InterlockedCoordIncrementer(camera.Width, camera.Height);

            Action<int> act = (i) =>
            {
                var l = new Line(startPoint, tracePoint);
                res.SetPixel(i, (int) tracePoint.y + maxY + 1, TraceSingleRay(scene, l));
            };

            Action<int> outerCycle = (i) =>
            {
                var currTracePoint = new Point3D(minX + i, minY, camera.Z);
                for (int j = 0; j < camera.Height; ++j, ++tracePoint.y)
                {
                    var l = new Line(startPoint, tracePoint);
                    res.SetPixel(i, j, TraceSingleRay(scene, l));
                }
            };*/

            var swatch = new Stopwatch();
            swatch.Start();
            for (int i = 0; i < camera.Width; ++tracePoint.x, ++i)
            {
                tracePoint.y = maxY;
                for (int j = 0; j < camera.Height; --tracePoint.y, ++j)
                {
                    var l = new Line(startPoint, tracePoint);
                    if ((i == 400) && (j == 220))
                        res.SetPixel(i, j, TraceSingleRay(scene, l));
                    else
                        res.SetPixel(i, j, TraceSingleRay(scene, l));
                }
            }
            //Parallel.For(0, camera.Width, outerCycle);
            swatch.Stop();
            MessageBox.Show(String.Format("{0}", swatch.ElapsedMilliseconds));
            return res;
        }

        private static Color TraceSingleRay(Scene scene, Line l)
        {
            var res = GetNearestIntersect(scene, l);
            if (res == null)
                return Color.FromArgb(0, 0, 0);

            var vec = new Vector3D(res.point, res.point + res.normal);

            int r = 0, g = 0, b = 0;
            double coef = 0;

            foreach (Point3D p in scene.LightSources)
            {
                Line shadowRay = new Line
                (
                    res.point,
                    p
                );

                var obst = GetNearestIntersect(scene, shadowRay);

                if (obst != null)
                    continue;

                coef += Math.Abs (shadowRay.CoefsVector.GetAngleCos(vec));
                if (coef >= 1)
                {
                    coef = 1;
                    break;
                }
            }

            r = checked ((int)Math.Round(coef * res.color.R));
            g = checked ((int)Math.Round(coef * res.color.G));
            b = checked ((int)Math.Round(coef * res.color.B));

            return Color.FromArgb(r, g, b);
        }

        private static RayDataColor GetNearestIntersect (Scene scene, Line l)
        {
            RayDataColor res = null;
            double? dist = null;
            double? currDist = null;

            foreach (DrawObject d in scene.Objects)
            {
                var curr = d.Intersects(l);
                if (curr != null)
                    currDist = Point3D.Distance(curr.point, l.CoefsVector.rVec);
                else
                    currDist = null;

                if (((res == null) && (currDist != null)) || ((res != null) && (currDist < dist)))
                {
                    res = curr;
                    dist = currDist;
                }
            }
                return res;
        }
    }
}
