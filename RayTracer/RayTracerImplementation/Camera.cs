﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

using Common;
using DrawObjects;

namespace RayTracerImplementation
{
    internal class Camera
    {
        public Camera(double angle, int width, int height)
        {
            Width = width;
            Height = height;

            Z = width / 2 / Math.Tan(angle / 2);
        }

        public readonly double Z;
        public readonly int Width;
        public readonly int Height;
    }
}
