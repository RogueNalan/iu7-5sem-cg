﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

using Microsoft.VisualBasic;

using Common;
using DrawObjects;

namespace RayTracer
{
    internal static class GuiInput
    {
        public static DrawSphere GetDrawSphere(Color color)
        {
            var center = GetPoint3D("Координаты центра");

            double rad = Convert.ToDouble (Interaction.InputBox("Введите радиус", "Ввод радиуса", "1"));

            return new DrawSphere(center, rad, color, 1, IncrementalID.Id);
        }

        public static Parallelipiped GetParallelipiped(Color color)
        {
            return new Parallelipiped(
                                        GetPoint3D("Первая точка"),
                                        GetPoint3D("Вторая точка"),
                                        GetPoint3D("Третья точка"),
                                        GetPoint3D("Смещение"),
                                        color,
                                        1,
                                        IncrementalID.Id
                                     );
        }

        public static Prism GetPrism(Color color)
        {
            return new Prism(
                                GetPoint3D("Первая точка"),
                                GetPoint3D("Вторая точка"),
                                GetPoint3D("Третья точка"),
                                GetPoint3D("Смещение"),
                                color,
                                1,
                                IncrementalID.Id
                             );
        }

        public static Pyramid3D GetPyramid3D(Color color)
        {
            return new Pyramid3D(
                                    GetPoint3D("Первая точка основания"),
                                    GetPoint3D("Вторая точка основания"),
                                    GetPoint3D("Третья точка основания"),
                                    GetPoint3D("Вершина"),
                                    color,
                                    1,
                                    IncrementalID.Id
                                );
        }

        public static Point3D GetPoint3D (string message)
        {
            string data = Interaction.InputBox(message, "Ввод точки", "0 0 0");

            return new Point3D(data);
        }
        
        public static int GetId()
        {
            return Convert.ToInt32(Interaction.InputBox("Введите идентификатор", "Ввод идентификатора", "0"));
        }

        public static double GetAngle()
        {
            return Convert.ToDouble(Interaction.InputBox("Введите угол", "Ввод угла", "0"));
        }

        private static double GetReflCoef()
        {
            return Convert.ToDouble(Interaction.InputBox("Введите коэф. отражения", "Ввод коэф. отражения", "1"));
        }
    }

    internal static class IncrementalID
    {
        public static int Id
        {
            get
            {
                return _id++;
            }
            set
            {
                _id = value;
            }
        }

        private static int _id = 0;
    }
}
