﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;

using DrawObjects;
using RayTracerImplementation;

namespace RayTracer
{
    public partial class fRayTracer : Form
    {
        public fRayTracer()
        {
            InitializeComponent();
        }

        private void btnCreateObject_Click(object sender, EventArgs e)
        {
            DrawObject add = null;

            int choice = cbObject.SelectedIndex;

            Color color = btnColor.BackColor;

            try
            {
                switch (choice)
                {
                    case 0:
                        add = GuiInput.GetPyramid3D(color);
                        break;
                    case 1:
                        add = GuiInput.GetPrism(color);
                        break;
                    case 2:
                        add = GuiInput.GetDrawSphere(color);
                        break;
                    case 3:
                        add = GuiInput.GetParallelipiped(color);
                        break;
                }
            }
           catch (ArgumentException exc)
            {
                MessageBox.Show(String.Format("Введены неверные данные: {0}", exc.Message));
                return;
            }
           catch (Exception)
            {
                MessageBox.Show("Произошла непредвиденная ошибка!");
                return;
            }

            Controller.AddDrawObject(add);
            MessageBox.Show(String.Format("Объект с ID = {0} был добавлен!", add.Id));
        }

        private void btnColor_Click(object sender, EventArgs e)
        {
            cdObjectColor.ShowDialog();
            btnColor.BackColor = cdObjectColor.Color;
        }

        private void btnAddLS_Click(object sender, EventArgs e)
        {
            var coord = GuiInput.GetPoint3D("Введите координаты источника света");

            Controller.AddLightSource(coord);
        }

        private void btnDeleteObject_Click(object sender, EventArgs e)
        {
            Controller.DeleteObject(GuiInput.GetId());
        }

        private RayTracerController Controller 
        {
            get
            {
                if (_controller == null)
                    _controller = new RayTracerController
                    (
                        pbImage,
                        pbImage.Width,
                        pbImage.Height
                    );

                return _controller;
            }
            set
            {
                _controller = value;
            }
        }

        private DrawObjectFileInput Loader
        {
            get
            {
                if (_loader == null)
                    _loader = new DrawObjectFileInput(Controller);
                return _loader;
            }
            set 
            {
                _loader = value;
            }
        }

        private RayTracerController _controller = null;
        private DrawObjectFileInput _loader = null;

        private void btnRender_Click(object sender, EventArgs e)
        {
            Controller.Render();
        }

        private void btnLoadObjects_Click(object sender, EventArgs e)
        {
            try
            {
                if (ofdObjects.ShowDialog() == DialogResult.OK)
                {
                    Loader.Load(ofdObjects.FileName);
                    MessageBox.Show("Объекты успешно загружены!");
                }
                else
                    MessageBox.Show("Файл указан неверно!");
            }
            catch(Exception)
            {
                MessageBox.Show("Возникла ошибка при загрузке!");
            }
        }

        private void btnRotateObject_Click(object sender, EventArgs e)
        {
            int id = GuiInput.GetId();
            var axis = GuiInput.GetPoint3D("Введите ось вращения");
            double angle = GuiInput.GetAngle();

            Controller.RotateObject(id, axis.x, axis.y, axis.z, angle);
        }

        private void btnMoveSingle_Click(object sender, EventArgs e)
        {
            int id = GuiInput.GetId();
            var move = GuiInput.GetPoint3D("Введите значение для сдвига");
            Controller.MoveObject(id, move.x, move.y, move.z);
        }

        private void btnMoveAll_Click(object sender, EventArgs e)
        {
            var move = GuiInput.GetPoint3D("Введите значение для сдвига");
            Controller.MoveAll(move.x, move.y, move.z);
        }

        private void btnRotateAll_Click(object sender, EventArgs e)
        {
            var axis = GuiInput.GetPoint3D("Введите ось вращения");
            double angle = GuiInput.GetAngle();

            Controller.RotateAll(axis.x, axis.y, axis.z, angle);
        }
    }
}
