﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Drawing;

using Common;
using DrawObjects;
using RayTracerImplementation;

namespace RayTracer
{
    internal class DrawObjectFileInput
    {
        public DrawObjectFileInput(RayTracerController controller)
        {
            if (controller == null)
                throw new ArgumentException();

            this.controller = controller;
        }

        public void Load(string filename)
        {
            using (StreamReader file = new StreamReader(filename))
            {
                string s;
                while ((s = file.ReadLine()) != null)
                {
                    if (s == "\n")
                        continue;
                    int code = Convert.ToInt32(s);

                    switch(code)
                    {
                        case 0: AddPyramid(file);           break;
                        case 1: AddPrism(file);             break;
                        case 2: AddSphere(file);            break;
                        case 3: AddParallelipiped(file);    break;
                        case 4: AddLightSource(file);       break;
                    }
                }
            }
        }

        private void AddPyramid(StreamReader file)
        {
            controller.AddDrawObject(new Pyramid3D
            (
                new Point3D(file.ReadLine()),
                new Point3D(file.ReadLine()),
                new Point3D(file.ReadLine()),
                new Point3D(file.ReadLine()),
                GetColor(file.ReadLine()),
                1,
                IncrementalID.Id
            ));
        }

        private void AddPrism(StreamReader file)
        {
            controller.AddDrawObject(new Prism
            (
                new Point3D(file.ReadLine()),
                new Point3D(file.ReadLine()),
                new Point3D(file.ReadLine()),
                new Point3D(file.ReadLine()),
                GetColor(file.ReadLine()),
                1,
                IncrementalID.Id
            ));
        }

        private void AddSphere(StreamReader file)
        {
            var p = new Point3D(file.ReadLine());
            double rad = Convert.ToDouble(file.ReadLine());
            Color col = GetColor(file.ReadLine());

            controller.AddDrawObject(new DrawSphere(p, rad, col, 1, IncrementalID.Id));
        }

        private void AddParallelipiped(StreamReader file)
        {
            controller.AddDrawObject(new Parallelipiped
            (
                new Point3D(file.ReadLine()),
                new Point3D(file.ReadLine()),
                new Point3D(file.ReadLine()),
                new Point3D(file.ReadLine()),
                GetColor(file.ReadLine()),
                1,
                IncrementalID.Id
            ));
        }

        private void AddLightSource(StreamReader file)
        {
            var coord = new Point3D(file.ReadLine());
            controller.AddLightSource(coord);
        }

        private static Color GetColor(string str)
        {
            string[] code = str.Split(' ');

            return Color.FromArgb(Convert.ToInt32(code[0]), Convert.ToInt32(code[1]), Convert.ToInt32(code[2]));
        }

        private RayTracerController controller;
    }
}