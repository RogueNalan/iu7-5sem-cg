﻿using System.Drawing;

using RayTracerImplementation;

namespace RayTracer
{
    partial class fRayTracer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbImage = new System.Windows.Forms.PictureBox();
            this.btnCreateObject = new System.Windows.Forms.Button();
            this.cbObject = new System.Windows.Forms.ComboBox();
            this.btnColor = new System.Windows.Forms.Button();
            this.btnAddLS = new System.Windows.Forms.Button();
            this.btnDeleteObject = new System.Windows.Forms.Button();
            this.cdObjectColor = new System.Windows.Forms.ColorDialog();
            this.btnRender = new System.Windows.Forms.Button();
            this.ofdObjects = new System.Windows.Forms.OpenFileDialog();
            this.btnLoadObjects = new System.Windows.Forms.Button();
            this.btnMoveSingle = new System.Windows.Forms.Button();
            this.btnMoveAll = new System.Windows.Forms.Button();
            this.btnRotateObject = new System.Windows.Forms.Button();
            this.btnRotateAll = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pbImage)).BeginInit();
            this.SuspendLayout();
            // 
            // pbImage
            // 
            this.pbImage.Location = new System.Drawing.Point(0, 0);
            this.pbImage.MaximumSize = new System.Drawing.Size(800, 480);
            this.pbImage.MinimumSize = new System.Drawing.Size(800, 480);
            this.pbImage.Name = "pbImage";
            this.pbImage.Size = new System.Drawing.Size(800, 480);
            this.pbImage.TabIndex = 0;
            this.pbImage.TabStop = false;
            // 
            // btnCreateObject
            // 
            this.btnCreateObject.Location = new System.Drawing.Point(807, 50);
            this.btnCreateObject.Name = "btnCreateObject";
            this.btnCreateObject.Size = new System.Drawing.Size(153, 23);
            this.btnCreateObject.TabIndex = 1;
            this.btnCreateObject.Text = "Создать объект";
            this.btnCreateObject.UseVisualStyleBackColor = true;
            this.btnCreateObject.Click += new System.EventHandler(this.btnCreateObject_Click);
            // 
            // cbObject
            // 
            this.cbObject.DisplayMember = "1";
            this.cbObject.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbObject.FormattingEnabled = true;
            this.cbObject.Items.AddRange(new object[] {
            "Пирамида",
            "Призма",
            "Сфера",
            "Куб"});
            this.cbObject.Location = new System.Drawing.Point(807, 13);
            this.cbObject.Name = "cbObject";
            this.cbObject.Size = new System.Drawing.Size(123, 21);
            this.cbObject.TabIndex = 2;
            // 
            // btnColor
            // 
            this.btnColor.BackColor = System.Drawing.Color.White;
            this.btnColor.Location = new System.Drawing.Point(936, 13);
            this.btnColor.Name = "btnColor";
            this.btnColor.Size = new System.Drawing.Size(24, 23);
            this.btnColor.TabIndex = 3;
            this.btnColor.UseVisualStyleBackColor = false;
            this.btnColor.Click += new System.EventHandler(this.btnColor_Click);
            // 
            // btnAddLS
            // 
            this.btnAddLS.Location = new System.Drawing.Point(807, 79);
            this.btnAddLS.Name = "btnAddLS";
            this.btnAddLS.Size = new System.Drawing.Size(153, 23);
            this.btnAddLS.TabIndex = 4;
            this.btnAddLS.Text = "Создать источник света";
            this.btnAddLS.UseVisualStyleBackColor = true;
            this.btnAddLS.Click += new System.EventHandler(this.btnAddLS_Click);
            // 
            // btnDeleteObject
            // 
            this.btnDeleteObject.Location = new System.Drawing.Point(806, 153);
            this.btnDeleteObject.Name = "btnDeleteObject";
            this.btnDeleteObject.Size = new System.Drawing.Size(154, 23);
            this.btnDeleteObject.TabIndex = 6;
            this.btnDeleteObject.Text = "Удалить объект";
            this.btnDeleteObject.UseVisualStyleBackColor = true;
            this.btnDeleteObject.Click += new System.EventHandler(this.btnDeleteObject_Click);
            // 
            // btnRender
            // 
            this.btnRender.Location = new System.Drawing.Point(807, 448);
            this.btnRender.Name = "btnRender";
            this.btnRender.Size = new System.Drawing.Size(153, 23);
            this.btnRender.TabIndex = 7;
            this.btnRender.Text = "Построить изображение";
            this.btnRender.UseVisualStyleBackColor = true;
            this.btnRender.Click += new System.EventHandler(this.btnRender_Click);
            // 
            // btnLoadObjects
            // 
            this.btnLoadObjects.Location = new System.Drawing.Point(807, 109);
            this.btnLoadObjects.Name = "btnLoadObjects";
            this.btnLoadObjects.Size = new System.Drawing.Size(153, 38);
            this.btnLoadObjects.TabIndex = 8;
            this.btnLoadObjects.Text = "Загрузить объекты из файла";
            this.btnLoadObjects.UseVisualStyleBackColor = true;
            this.btnLoadObjects.Click += new System.EventHandler(this.btnLoadObjects_Click);
            // 
            // btnMoveSingle
            // 
            this.btnMoveSingle.Location = new System.Drawing.Point(807, 183);
            this.btnMoveSingle.Name = "btnMoveSingle";
            this.btnMoveSingle.Size = new System.Drawing.Size(153, 23);
            this.btnMoveSingle.TabIndex = 9;
            this.btnMoveSingle.Text = "Сдвинуть объект";
            this.btnMoveSingle.UseVisualStyleBackColor = true;
            this.btnMoveSingle.Click += new System.EventHandler(this.btnMoveSingle_Click);
            // 
            // btnMoveAll
            // 
            this.btnMoveAll.Location = new System.Drawing.Point(807, 213);
            this.btnMoveAll.Name = "btnMoveAll";
            this.btnMoveAll.Size = new System.Drawing.Size(153, 23);
            this.btnMoveAll.TabIndex = 10;
            this.btnMoveAll.Text = "Сдвинуть все объекты";
            this.btnMoveAll.UseVisualStyleBackColor = true;
            this.btnMoveAll.Click += new System.EventHandler(this.btnMoveAll_Click);
            // 
            // btnRotateObject
            // 
            this.btnRotateObject.Location = new System.Drawing.Point(807, 243);
            this.btnRotateObject.Name = "btnRotateObject";
            this.btnRotateObject.Size = new System.Drawing.Size(153, 23);
            this.btnRotateObject.TabIndex = 11;
            this.btnRotateObject.Text = "Повернуть объект";
            this.btnRotateObject.UseVisualStyleBackColor = true;
            this.btnRotateObject.Click += new System.EventHandler(this.btnRotateObject_Click);
            // 
            // btnRotateAll
            // 
            this.btnRotateAll.Location = new System.Drawing.Point(807, 272);
            this.btnRotateAll.Name = "btnRotateAll";
            this.btnRotateAll.Size = new System.Drawing.Size(153, 23);
            this.btnRotateAll.TabIndex = 12;
            this.btnRotateAll.Text = "Повернуть все объекты";
            this.btnRotateAll.UseVisualStyleBackColor = true;
            this.btnRotateAll.Click += new System.EventHandler(this.btnRotateAll_Click);
            // 
            // fRayTracer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(966, 483);
            this.Controls.Add(this.btnRotateAll);
            this.Controls.Add(this.btnRotateObject);
            this.Controls.Add(this.btnMoveAll);
            this.Controls.Add(this.btnMoveSingle);
            this.Controls.Add(this.btnLoadObjects);
            this.Controls.Add(this.btnRender);
            this.Controls.Add(this.btnDeleteObject);
            this.Controls.Add(this.btnAddLS);
            this.Controls.Add(this.btnColor);
            this.Controls.Add(this.cbObject);
            this.Controls.Add(this.btnCreateObject);
            this.Controls.Add(this.pbImage);
            this.Name = "fRayTracer";
            this.Text = "RayTracer";
            ((System.ComponentModel.ISupportInitialize)(this.pbImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pbImage;
        private System.Windows.Forms.Button btnCreateObject;
        private System.Windows.Forms.ComboBox cbObject;
        private System.Windows.Forms.Button btnColor;
        private System.Windows.Forms.Button btnAddLS;
        private System.Windows.Forms.Button btnDeleteObject;
        private System.Windows.Forms.ColorDialog cdObjectColor;
        private System.Windows.Forms.Button btnRender;
        private System.Windows.Forms.OpenFileDialog ofdObjects;
        private System.Windows.Forms.Button btnLoadObjects;
        private System.Windows.Forms.Button btnMoveSingle;
        private System.Windows.Forms.Button btnMoveAll;
        private System.Windows.Forms.Button btnRotateObject;
        private System.Windows.Forms.Button btnRotateAll;
    }
}

