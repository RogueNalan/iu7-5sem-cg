﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

using Common;
using Surfaces;

namespace DrawObjects
{
    public class DrawSphere: DrawObject
    {
        public DrawSphere(Point3D center, double rad, Color color, double reflCoef, int id):
            base (color, reflCoef, id)
        {
            try
            {
                sphereImpl = new Sphere(center, rad);
            }
            catch(ArgumentException)
            {
                throw new ArgumentException("DrawSphere creation failed!\n");
            }
        }

        public override void Move(double dx, double dy, double dz)
        {
            sphereImpl.Move(dx, dy, dz);
        }

        public override void Rotate(Quaternion q, Quaternion invQ)
        {
            sphereImpl.Rotate(q, invQ);
        }

        protected override List<RayData> GetAllIntersections(Line l)
        { 
            var temp = sphereImpl.Intersects(l);

            if (temp == null)
                return null;

            var res = new List<RayData>();

            foreach (Point3D p in temp)
                res.Add(new RayData(p, sphereImpl.Normal(p)));

            return res;
        }

        private Sphere sphereImpl { get; set; }
    }
}
