﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

using Common;
using Surfaces;

namespace DrawObjects
{
    public class Pyramid3D: DrawObject
    {
        public Pyramid3D(Point3D a, Point3D b, Point3D c, Point3D h, Color color, double reflCoef, int id):
            base (color, reflCoef, id)
        {
            double ab = Point3D.Distance(a, b);
            double bc = Point3D.Distance(b, c);
            double ac = Point3D.Distance(a, c);

            Point3D a1 = null, b1 = null, c1 = null, d1 = null;

            if ((ab > bc) && (ab > ac))
            {
                b1 = c;
                a1 = a;
                c1 = b;
            }
            else if (bc > ac)
            {
                a1 = b;
                b1 = a;
                c1 = c;
            }
            else
            {
                a1 = a;
                b1 = b;
                c1 = c;
            }

            d1 = a1 + (c1 - b1);

            rectangle = new Rectangle3D[]
            {
                new Rectangle3D(a, b, c)
            };

            polygons = new Polygon3D[]
            {
                new Polygon3D (a1, b1, h),
                new Polygon3D (b1, c1, h),
                new Polygon3D (c1, d1, h),
                new Polygon3D (a1, d1, h)
            };
        }

        public override void Move(double dx, double dy, double dz)
        {
            rectangle[0].Move(dx, dy, dz);

            foreach (Polygon3D p in polygons)
                p.Move(dx, dy, dz);
        }

        public override void Rotate(Common.Quaternion q, Common.Quaternion invQ)
        {
            rectangle[0].Rotate(q, invQ);

            foreach (Polygon3D p in polygons)
                p.Rotate(q, invQ);
        }

        protected override List<RayData> GetAllIntersections(Common.Line l)
        {
            List<RayData> res = new List<RayData>();

            DrawObject.CheckForIntersections(rectangle, l, res);
            DrawObject.CheckForIntersections(polygons, l, res);

            if (res.Count != 0)
                return res;
            else
                return null;
        }

        private Polygon3D[]     polygons  { get; set; }
        private Rectangle3D[]   rectangle { get; set; }
    }
}
