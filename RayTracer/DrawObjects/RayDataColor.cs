﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

using Common;
using Surfaces;

namespace DrawObjects
{
    public class RayDataColor: RayData
    {
        public RayDataColor(RayData data, Color color):
            base (data.point, data.normal)
        {
            this.color = color;
        }

        public Color color;
    }
}
