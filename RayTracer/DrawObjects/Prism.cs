﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

using Common;
using Surfaces;

namespace DrawObjects
{
    public class Prism: DrawObject
    {
        public Prism (Point3D a, Point3D b, Point3D c, Point3D offset, Color color, double reflCoef, int id):
            base (color, reflCoef, id)
        {
            if (CommonFunctions.CompareDouble(offset.x, 0) &&
                CommonFunctions.CompareDouble(offset.y, 0) &&
                CommonFunctions.CompareDouble(offset.z, 0))
                throw new ArgumentException("Prism creation failed (offset)!\n");

            Polygon3D p = null;

            try
            {
                p = new Polygon3D(a, b, c);
            }
            catch (ArgumentException)
            {
                throw new ArgumentException("Prism creation failed (polygon)!\n");
            }

            Point3D a1 = a + offset, b1 = b + offset, c1 = c + offset;

            polygons = new Polygon3D[]
            {
                p,
                new Polygon3D(a1, b1, c1)
            };

            rectangles = new Rectangle3D[]
            {
                new Rectangle3D (a, b, a1),
                new Rectangle3D(a, c, a1),
                new Rectangle3D(b, c, c1)
            };
        }

        public override void Move(double dx, double dy, double dz)
        {
            foreach (Polygon3D p in polygons)
                p.Move(dx, dy, dz);
            foreach (Rectangle3D r in rectangles)
                r.Move(dx, dy, dz);
        }

        public override void Rotate(Quaternion q, Quaternion invQ)
        {
            foreach (Polygon3D p in polygons)
                p.Rotate(q, invQ);
            foreach (Rectangle3D r in rectangles)
                r.Rotate(q, invQ);
        }


        protected override List<RayData> GetAllIntersections(Line l)
        {
            var res = new List<RayData>();

            DrawObject.CheckForIntersections(polygons, l, res);
            DrawObject.CheckForIntersections(rectangles, l, res);

            if (res.Count == 0)
                res = null;

            return res;
        }

        private Polygon3D[] polygons { get; set; }
        private Rectangle3D[] rectangles { get; set; }
    }
}
