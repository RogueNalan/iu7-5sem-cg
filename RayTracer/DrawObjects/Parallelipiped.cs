﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;


using Common;
using Surfaces;

namespace DrawObjects
{
    public class Parallelipiped: DrawObject
    {
        public Parallelipiped(Point3D a, Point3D b, Point3D c, Point3D offset, Color color, double reflCoef, int id):
            base (color, reflCoef, id)
        {
            if (CommonFunctions.CompareDouble(offset.x, 0) &&
                 CommonFunctions.CompareDouble(offset.y, 0) &&
                 CommonFunctions.CompareDouble(offset.z, 0))
                throw new ArgumentException("Wrong data in Parallelipiped constuctor!\n");

            double ab = Point3D.Distance(a, b);
            double bc = Point3D.Distance(b, c);
            double ac = Point3D.Distance(a, c);

            Point3D a1 = null, b1 = null, c1 = null, d1 = null,
                    a2 = null, b2 = null, c2 = null;

            if ((ab > bc) && (ab > ac))
            {
                b1 = c;
                a1 = a;
                c1 = b;
            }
            else if (bc > ac)
            {
                a1 = b;
                b1 = a;
                c1 = c;
            }
            else
            {
                a1 = a;
                b1 = b;
                c1 = c;
            }

            d1 = a1 + (c1 - b1);

            a2 = a1 + offset;
            b2 = b1 + offset;
            c2 = c1 + offset;

            try
            {
                rectangles = new Rectangle3D[]
                {
                    new Rectangle3D(a1, b1, c1),
                    new Rectangle3D(a1, b1, a2),
                    new Rectangle3D(b1, c1, b2),
                    new Rectangle3D(c1, d1, c2),
                    new Rectangle3D(a1, d1, a2),
                    new Rectangle3D(a2, b2, c2)
                };
            }
            catch (ArgumentException)
            {
                throw new ArgumentException("Wrong data in Parallelipiped constuctor!\n");
            }
        }

        public override void Move(double dx, double dy, double dz)
        {
            foreach (Rectangle3D r in rectangles)
                r.Move(dx, dy, dz);
        }

        public override void Rotate(Quaternion q, Quaternion invQ)
        {
            foreach (Rectangle3D r in rectangles)
                r.Rotate(q, invQ);
        }

        protected override List<RayData> GetAllIntersections(Line l)
        {
            List<RayData> res = new List<RayData>();

            DrawObject.CheckForIntersections(rectangles, l, res);

            if (res.Count == 0)
                res = null;

            return res;
        }

        private Rectangle3D[] rectangles { get; set; }
    }
}
