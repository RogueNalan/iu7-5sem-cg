﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

using Common;
using Surfaces;

namespace DrawObjects
{
    public abstract class DrawObject: ISceneObject
    {
        public DrawObject(Color color, double reflCoef, int id)
        {
            this.color = color;
            this.reflCoef = reflCoef;
            this.Id = id;
        }

        public abstract void Move(double dx, double dy, double dz);
        public abstract void Rotate(Quaternion q, Quaternion invQ);

        public RayDataColor Intersects(Line l)
        {
            var list = GetAllIntersections(l);

            if (list == null)
                return null;

            RayData res = null;
            double? dist = null;

            var rVec = l.CoefsVector.rVec;
            var offset = l.CoefsVector.offset;

            foreach (RayData r in list)
            {
                double curr = Point3D.Distance(r.point, rVec);

                if (((dist == null) || (curr < dist)) && (GetCoef (r.point - l.CoefsVector.rVec, offset) > 1e-7))
                    {
                        res = r;     
                        dist = curr;
                    }
            }

            if (res != null)
                return new RayDataColor(res, color);
            else
                return null;
        }

        protected abstract List<RayData> GetAllIntersections(Line l);

        protected static void CheckForIntersections (IOneIntersection[] arr, Line l, List<RayData> res)
        {
            RayData temp = null;

            foreach (IOneIntersection s in arr)
            {
                temp = s.Intersects(l);
                if (temp != null)
                    res.Add(temp);
            }
        }

        private double GetCoef(Point3D a, Point3D b)
        {
            if (!CommonFunctions.CompareDouble(b.x, 0))
                return a.x / b.x;

            if (!CommonFunctions.CompareDouble(b.y, 0))
                return a.y / b.y;

            return a.z / b.z;
        }

        protected Color color;
        public double reflCoef { get; protected set; }
        public readonly int Id;
    }
}
