﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Common;

namespace Tests
{
    [TestClass]
    public class CommonTests
    {
        [TestMethod]
        public void PointRotateTest()
        {
            var p = new Point3D()
            {
                x = 5,
                y = 3,
                z = 2.5
            };

            var nullQ = new Quaternion(new Point3D(0, 0, 0), 0);

            var firstQ = new Quaternion
                (
                    new Point3D()
                    {
                        x = 1,
                        y = 0,
                        z = 0
                    },
                    Math.PI * 0.25
                );

            var t1 = nullQ.Multiply(firstQ);

            var t2 = new Quaternion
            (
                new Point3D()
                {
                    x = 1,
                    y = 0,
                    z = 0
                },
                Math.PI * 0.5
            );

            var q = t1.Multiply(t2);

            var invQ = q.Conjugation();

            p.Rotate(q, invQ);

            const double eps = 1e-4;

            Assert.IsTrue(Math.Abs(p.x - 5) < eps, "3 * PI / 4 rotation on x axis, x coordinate failed");
            Assert.IsTrue(Math.Abs(p.y + 0.3535) < eps, "3 * PI / 4 rotation on x axis, y coordinate failed");
            Assert.IsTrue(Math.Abs(p.z + 3.8890) < eps, "3 * PI / 4 rotation on x axis, z coordinate failed");
        }

        [TestMethod]
        public void VectorRotateTest()
        {
            var v = new Vector3D
            (
                new Point3D() { x = 1, y = 0, z = 0 },
                new Point3D() { x = 5, y = 3, z = 2.5 }
            );

            var q = new Quaternion
            (
                new Point3D()
                {
                    x = 1,
                    y = 0,
                    z = 0
                },
                Math.PI * 0.75
            );

            var invQ = q.Conjugation();

            v.Rotate(q, invQ);

            var p = v.rVec + v.offset;

            const double eps = 1e-4;

            Assert.IsTrue(Math.Abs(p.x - 5) < eps, "3 * PI / 4 rotation on x axis, x coordinate failed");
            Assert.IsTrue(Math.Abs(p.y + 0.3535) < eps, "3 * PI / 4 rotation on x axis, y coordinate failed");
            Assert.IsTrue(Math.Abs(p.z + 3.8890) < eps, "3 * PI / 4 rotation on x axis, z coordinate failed");
        }

        [TestMethod]
        public void LinePointCheckTest()
        {
            var l = new Line
                (
                    new Point3D()
                    {
                        x = 1,
                        y = 2,
                        z = 3
                    },
                    new Point3D()
                    {
                        x = 4,
                        y = 5,
                        z = 6
                    }
                );

            var test1 = new Point3D()
            {
                x = 7,
                y = 8,
                z = 9
            };

            var test2 = new Point3D()
            {
                x = 0,
                y = 0,
                z = 1
            };

            Assert.IsTrue(l.Check(test1), "Must be true");
            Assert.IsFalse(l.Check(test2), "Must be false");
        }

        [TestMethod]
        public void CheckPointOnSurfaceTest()
        {
            var s = new Plane
                (
                    new Point3D()
                    {
                        x = 0,
                        y = 0,
                        z = 0
                    },
                    new Point3D()
                    {
                        x = 1,
                        y = 1,
                        z = 1
                    },
                    new Point3D()
                    {
                        x = 1,
                        y = 0,
                        z = 0
                    }
                );

            var p = new Point3D()
            {
                x = 2,
                y = 0,
                z = 0
            };

            Assert.IsTrue(s.CheckPoint(p), "This point belongs to surface");

            p.x = 2;
            p.y = 1;
            p.z = 0;

            Assert.IsFalse(s.CheckPoint(p), "This point doesn't belong to surface");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void SurfaceCreationFailTest()
        {
            var s = new Plane
            (
                new Point3D()
                {
                    x = 1,
                    y = 2,
                    z = 3
                },
                new Point3D()
                {
                    x = 4,
                    y = 5,
                    z = 6
                },
                new Point3D()
                {
                    x = 7,
                    y = 8,
                    z = 9
                }
            );
        }

        [TestMethod]
        public void SurfaceIntersectionTest()
        {
            var s = new Plane
            (
                new Point3D()
                {
                    x = 0,
                    y = 0,
                    z = 0
                },
                new Point3D()
                {
                    x = 1,
                    y = 0,
                    z = 0
                },
                new Point3D()
                {
                    x = 0,
                    y = 1,
                    z = 0
                }
            );

            var l = new Line
            (
                new Point3D()
                {
                    x = 2,
                    y = 2,
                    z = 2
                },
                new Point3D()
                {
                    x = 2,
                    y = 2,
                    z = 1
                }
            );

            Point3D expected = new Point3D()
            {
                x = 2,
                y = 2,
                z = 0
            };

            Assert.AreEqual(expected, s.Intersects(l), "Line intersects surface");

            l = new Line(new Point3D(0, 0, 0), new Point3D(0, 1, 0));
            Assert.IsNull(s.Intersects(l), "Line belongs the surface");

            l = new Line(new Point3D(0, 0, 1), new Point3D(0, 1, 1));
            Assert.IsNull(s.Intersects(l), "Line is parallel to the surface");
        }
    }
}