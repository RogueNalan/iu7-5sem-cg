﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Common;
using Surfaces;

namespace Tests
{
    [TestClass]
    public class SurfacesTests
    {
        [TestMethod]
        public void PolygonCheckTest()
        {
            var p = new Polygon3D
            (
                new Point3D(0, 0, 0),
                new Point3D(0, 0, 1),
                new Point3D(1, 1, 0)
            );

            var check1 = new Point3D(0.1, 0.1, 0.5);
            var check2 = new Point3D(-50, -50, -50);
            var check3 = new Point3D(0, 0, 0);
            var check4 = new Point3D(0.5,0.5,0);

            Assert.IsTrue(p.Check(check1), "Point belongs to polygon, test 1!\n");
            Assert.IsFalse(p.Check(check2), "Point doesn't belong to polygon, test 2!\n");
            Assert.IsFalse(p.Check(check3), "Point is on polygon edge, test 3!\n");
            Assert.IsFalse(p.Check(check4), "Point is on polygon edge, test4!\n");
        }
        [TestMethod]
        public void SphereIntersectionTests()
        {
            var s = new Sphere
            (
                new Point3D(0, 0, 0),
                10
            );

            var l1 = new Line
            (
                new Point3D(0, 0, -12),
                new Point3D(0, 0, -11)
            );

            var l2 = new Line
            (
                new Point3D(25, 25, 25),
                new Point3D(26, 25, 25)
            );

            var test = new Point3D(0, 0, -10);
            var testPoints = s.Intersects(l1);


            Assert.IsTrue((test == testPoints[0]) || (test == testPoints[1]), "Best intersection at (0,0,-10)");
            Assert.IsNull(s.Intersects(l2), "No intersection there");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void RectangleFailCreationTest()
        {
            var rec = new Rectangle3D
            (
                new Point3D(0, 0, 0),
                new Point3D(1, 1, 1),
                new Point3D(1.1, 0.5, 0)
            );
        }

        [TestMethod]
        public void RectangleCheckTest()
        {
            var rec = new Rectangle3D
            (
                new Point3D(0, 0, 0),
                new Point3D(0, 0, 1),
                new Point3D(1, 0, 1)
            );

            var p1 = new Point3D(0.5, 0, 0.5);
            var p2 = new Point3D(1, 0, 0);

            Assert.IsTrue(rec.Check(p1), "Point belongs to rectangle");
            Assert.IsFalse(rec.Check(p2), "Point doesn't belong to rectangle");
        }
    }
}
